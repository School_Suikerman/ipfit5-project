import hashlib
import pytsk3
import pyewf
from beautifultable import BeautifulTable
from zipfile import *
import logging


logging.basicConfig(filename='Algemeen.log',
                    level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(name)s - %(message)s')


class EWFImgInfo(pytsk3.Img_Info):
    def __init__(self, ewf_handle):
        self._ewf_handle = ewf_handle
        super(EWFImgInfo, self).__init__(
            url="", type=pytsk3.TSK_IMG_TYPE_EXTERNAL)

    def close(self):
        self._ewf_handle.close()

    def read(self, offset, size):
        self._ewf_handle.seek(offset)
        return self._ewf_handle.read(size)

    def get_size(self):
        return self._ewf_handle.get_media_size()

class Image_option:

    def __init__(self):
        pass

    def get_image(self):
        try:
            self.image_path = str(raw_input("Image path: "))
            self.filenames = pyewf.glob(self.image_path)

            self.ewf_handle = pyewf.handle()
            self.ewf_handle.open(self.filenames)

            global img_info
            img_info = EWFImgInfo(self.ewf_handle)

            global vol
            vol = pytsk3.Volume_Info(img_info)

            logging.info('Image succesvol ingeladen')
        except Exception:
            print "ERROR: Image kon niet worden ingeladen"
            logging.critical('Image kon niet worden ingeladen')
            exit()

    def show_files(self):
        table = BeautifulTable()
        table.column_headers = ['Size', 'Filename', 'Hash']

        for part in vol:
            if part.len > 2048 and "Unallocated" not in part.desc \
                    and "Extended" not in part.desc \
                    and "Primary Table" not in part.desc:
                try:
                    fs = pytsk3.FS_Info(img_info, offset=part.start * vol.info.block_size)
                except IOError:
                    _, e, _ = sys.exc_info()
                    print("[-] Unable to open FS:\n {}".format(e))
                root = fs.open_dir(path="/")

                for fs_object in root:
                    try:
                        file_size = getattr(fs_object.info.meta, "size", 0)
                        file_name = fs_object.info.name.name

                        hash_obj = hashlib.md5()
                        hash_obj.update(fs_object.read_random(0, file_size))
                        hash = hash_obj.hexdigest()

                        table.append_row([str(file_size), file_name, hash])

                    except Exception:
                        pass
                print table
                logging.info('Image succesvol uitgelezen en alle files met hash in een tabel gezet')
                logging.info('Programma gestopt')

    def list_zip(self):
        table = BeautifulTable()
        table.column_headers = ['Zip name', 'Files']

        for part in vol:
            if part.len > 2048 and "Unallocated" not in part.desc \
                    and "Extended" not in part.desc \
                    and "Primary Table" not in part.desc:
                try:
                    fs = pytsk3.FS_Info(img_info, offset=part.start * vol.info.block_size)
                except IOError:
                    _, e, _ = sys.exc_info()
                    print("[-] Unable to open FS:\n {}".format(e))
                root = fs.open_dir(path="/")

                for fs_object in root:
                    try:
                        file_name = fs_object.info.name.name
                        pattern = '2_files.zip'

                        if file_name == pattern:
                            zip_archive = ZipFile(pattern, 'r')
                            itemlist = zip_archive.namelist()
                            namelist = ", ".join(itemlist)

                            table.append_row([file_name, namelist])
                            zip_archive.close()

                    except Exception:
                        pass
                print table


    def bestand_menu(self):
        print "Bestand menu"
        print ""
        print "Optie 1: Laat alle bestanden zien en hash deze"
        print "Optie 2: Laat inhoud van samengestelde bestanden zien"
        print ""

        img3 = Image_option()
        choice = int(raw_input("Kies een optie: "))
        if choice == 1:
            logging.info('Gebruiker gekozen voor optie 1: Laat alle bestanden zien en hash deze')
            img3.show_files()
        if choice == 2:
            logging.info('Gebruiker gekozen voor optie 2: Laat inhoud van samengestelde bestanden zien')
            img3.list_zip()


