import datetime
import sqlite3
import os
import logging
from beautifultable import BeautifulTable

from Bestand import Image_option
# from IP import ...
# from Foto import ...


logging.basicConfig(filename='Algemeen.log',
                    level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(name)s - %(message)s')


class Database:

    table = BeautifulTable()
    table.column_headers = ['ID', 'Case name', 'Creation date', 'Examiner']

    def __init__(self):
        self.case_id = ""
        self.case_name = ""
        self.case_date = ""
        self.case_examiner = ""

        self.db = sqlite3.connect('database.db')
        self.cursor = self.db.cursor()

    def user_input(self):
        self.case_id = str(raw_input("ID: \t\t"))
        self.case_name = str(raw_input("Case: \t\t"))
        self.case_date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M")
        if self.case_date:
            print "Date: \t\t", self.case_date
        self.case_examiner = str(raw_input("Examinor: \t"))

        logging.info("Gebruiker voert case-gegevens in")

    def db_insert(self):
        try:
            self.cursor.execute('INSERT INTO Casus(case_id, case_name, creation_date, examiner) VALUES(?,?,?,?)',
                              (self.case_id, self.case_name, self.case_date, self.case_examiner))
            self.db.commit()
            logging.info(
                'Case created:\t {} : {} : {} : {}'.format(self.case_id, self.case_name, self.case_date, self.case_examiner))
            logging.info('Case info succesful sent to the database')
            print "Gegevens verstuurd!\n"
        except Exception:
            logging.critical('Entered existing Case ID, program quit')
            print "Case ID bestaat al! Probeer opnieuw ..."
            exit()

        self.table.append_row([self.case_id, self.case_name, self.case_date, self.case_examiner])

    def db_delete(self):

        choice = raw_input("Welke casus moet verwijderd worden? [ID] ")
        try:
            self.cursor.execute("DELETE FROM Casus WHERE case_id= '" + choice + "';")

            self.db.commit()

            logging.info('Record {} verwijderd uit de database'.format(self.case_id))
        except Exception as e:
            logging.info('Record {} kon niet worden verwijderd uit de database'.format(self.case_id))

    def db_show_cases(self):
        try:
            self.cursor.execute("SELECT * FROM Casus;")
        except Exception as e:
            print e

        rows = self.cursor.fetchall()
        for row in rows:
            self.table.append_row([row[0], row[1], row[2], row[3]])
        print self.table
        logging.info('All saved cases are shown')

    def db_delete_all(self):
        try:
            self.cursor.execute("DELETE FROM Casus;")
            self.db.commit()
        except Exception as e:
            print e

    def hoofdmenu(self):
        # os.system('clear') and os.system('cls')
        logging.info('+++++++++++++++++++ Programma gestart +++++++++++++++++++')
        print "Main menu"
        print ""
        print "Optie 1: \t Case aanmaken"
        print "Optie 2: \t Case inladen"
        print "Optie 3: \t Bestaande case verwijderen"
        print "Optie 4: \t Show all cases\n"
        print "Optie 5: \t ALLES CASES VERWIJDEREN [CREATOR ONLY]"

        choice = int(raw_input("Nieuwe case aanmaken, inladen of verwijderen?: "))
        if choice == 1:
            logging.info('Gebruiker gekozen voor optie 1: Case aanmaken')
            db1.user_input()
            db1.db_insert()

            img1 = Image_option()
            img1.get_image()

            db1.submenu()
        elif choice == 2:
            logging.info('Gebruiker gekozen voor optie 2: Case inladen')
            print "not ready yet..."
        elif choice == 3:
            logging.info('Gebruiker gekozen voor optie 3: Bestaande case verwijderen')
            db1.db_show_cases()
            db1.db_delete()
            exit()
        elif choice == 4:
            logging.info('Gebruiker gekozen voor optie 4: Show all cases')
            db1.db_show_cases()
            db1.hoofdmenu()
        elif choice == 5:
            db1.db_delete_all()
            exit()
        else:
            print "ERROR: Geen geldige optie gekozen"
            exit()

    def submenu(self):
        print "Casus: \t --> \t'" + self.case_name + "'"
        print ""
        print "Optie 1: \t Bestanden"
        print "Optie 2: \t Foto's"
        print "Optie 3: \t IP"
        print "Optie 4: \t Stop programma"
        print ""

        choice = int(raw_input("Kies een optie: "))
        if choice == 1:
            logging.info('Gebruiker gekozen voor optie 1: Bestanden')
            img2 = Image_option()
            img2.bestand_menu()
        elif choice == 2:
            logging.info("Gebruiker gekozen voor optie 2: Foto's")
        elif choice == 3:
            logging.info('Gebruiker gekozen voor optie 3: IP')
        elif choice == 4:
            logging.info('Gebruiker gekozen voor optie 4: Stop programma')
        else:
            logging.info('Ongeldige invoer')


db1 = Database()
db1.hoofdmenu()




















